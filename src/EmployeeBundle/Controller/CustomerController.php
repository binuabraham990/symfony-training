<?php

namespace EmployeeBundle\Controller;

use EmployeeBundle\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use EmployeeBundle\Form\CustomerType;

/**
 * Customer controller.
 *
 */
class CustomerController extends Controller {

    /**
     * Lists all customer entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $customers = $em->getRepository('EmployeeBundle:Customer')->findAll();

        return $this->render('EmployeeBundle:Customer:index.html.twig', array(
                    'customers' => $customers,
        ));
    }

    /**
     * Creates a new customer entity.
     *
     */
    public function newAction(Request $request) {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('EmployeeBundle:Customer:new.html.twig', array(
                    'customer' => $customer,
                    'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing customer entity.
     *
     */
    public function editAction(Request $request, Customer $customer) {
        $editForm = $this->createForm(CustomerType::class, $customer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('EmployeeBundle:Customer:edit.html.twig', array(
                    'customer' => $customer,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a customer entity.
     *
     */
    public function deleteAction(Request $request, Customer $customer) {
        if ($customer) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($customer);
            $em->flush();
        }

        return $this->redirectToRoute('customer_index');
    }

}
