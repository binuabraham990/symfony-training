<?php

namespace EmployeeBundle\Controller;

use EmployeeBundle\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EmployeeBundle\Form\CountryType;

/**
 * Country controller.
 *
 */
class CountryController extends Controller {

    /**
     * Lists all country entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $countries = $em->getRepository('EmployeeBundle:Country')->findAll();

        return $this->render('EmployeeBundle:Country:index.html.twig', array(
                    'countries' => $countries,
        ));
    }

    /**
     * Creates a new country entity.
     *
     */
    public function newAction(Request $request) {
        $country = new Country();
        $form = $this->createForm(CountryType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();

            return $this->redirectToRoute('country_index');
        }

        return $this->render('EmployeeBundle:Country:new.html.twig', array(
                    'country' => $country,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing country entity.
     *
     */
    public function editAction(Request $request, Country $country) {
        $editForm = $this->createForm(CountryType::class, $country);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('country_index');
        }

        return $this->render('EmployeeBundle:Country:edit.html.twig', array(
                    'country' => $country,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a country entity.
     *
     */
    public function deleteAction(Country $country) {
        if ($country) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($country);
            $em->flush();
        }

        return $this->redirectToRoute('country_index');
    }

    public function queriesAction() {

        $em = $this->getDoctrine()->getManager();
        $find = $em->getRepository('EmployeeBundle:Country')->find(1);

        $findOneBy = $em->getRepository('EmployeeBundle:Country')->findOneBy(['code' => 'IN']);
        
        
        $findBy = $em->getRepository('EmployeeBundle:State')->findBy(['country' => $findOneBy]);
        
        $findAll = $em->getRepository('EmployeeBundle:Country')->findAll();
        
        return $this->render('EmployeeBundle:Country:queries.html.twig', array(
            'find'  =>  $find,
            'findOneBy' =>  $findOneBy,
            'findBy'    =>  $findBy,
            'findAll'   =>  $findAll,
        ));
    }
    
    public function queryBuilderAction()    {
        
        $em = $this->getDoctrine()->getManager();
        $queryResult = $em->getRepository('EmployeeBundle:Country')->getByQueryBuilder();
        return $this->render('EmployeeBundle:Country:query_builder.html.twig', array(
            'result' => $queryResult,
        ));
    }
    
    public function serviceAction() {
        
        $message = $this->container->get('employee.message')->getHappyMessage();
        dump($message);
        die;
    }

}
