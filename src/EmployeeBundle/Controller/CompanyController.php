<?php

namespace EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\{
    Request,
    JsonResponse
};
use EmployeeBundle\Form\CompanyType;
use EmployeeBundle\Entity\Company;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CompanyController extends Controller {

    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $company = new Company();
        $form = $this->createAddForm($company);
        $companies = $em->getRepository('EmployeeBundle:Company')->findAll();
        return $this->render('EmployeeBundle:Company:index.html.twig', array(
                    'form' => $form->createView(),
                    'companies' => $companies,
        ));
    }

    public function saveAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $status = 'error';
        $dataView = '';
        $company = new Company();
        $form = $this->createAddForm($company);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($company);
            $em->flush();
            $status = 'success';
            $companies = $em->getRepository('EmployeeBundle:Company')->findAll();

            $company = new Company();
            $form = $this->createAddForm($company);
            $dataView = $this->renderView('EmployeeBundle:Company:list_content.html.twig', array(
                'companies' => $companies,
            ));
        }

        $formView = $this->renderView('EmployeeBundle:Company:form_content.html.twig', array(
            'form' => $form->createView(),
        ));
        return new JsonResponse(['status' => $status, 'formView' => $formView, 'dataView' => $dataView]);
    }

    public function editAction(Request $request) {

        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('EmployeeBundle:Company')->find($id);
        if (!$company) {
            return new JsonResponse(['status' => 'error', 'msg' => 'Some error occured.']);
        }
        $form = $this->createEditForm($company);
        $formView = $this->renderView('EmployeeBundle:Company:form_content.html.twig', array(
            'form' => $form->createView(),
        ));
    
        return new JsonResponse(['status' => 'success', 'formView' => $formView]);
    }

    public function updateAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $status = 'error';
        $dataView = '';
        $company = $em->getRepository('EmployeeBundle:Company')->find($id);
        $form = $this->createEditForm($company);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->flush();
            $status = 'success';
            $companies = $em->getRepository('EmployeeBundle:Company')->findAll();

            $company = new Company();
            $form = $this->createAddForm($company);
            $dataView = $this->renderView('EmployeeBundle:Company:list_content.html.twig', array(
                'companies' => $companies,
            ));
        }

        $formView = $this->renderView('EmployeeBundle:Company:form_content.html.twig', array(
            'form' => $form->createView(),
        ));
        return new JsonResponse(['status' => $status, 'formView' => $formView, 'dataView' => $dataView]);
    }

    public function deleteAction(Request $request) {

        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('EmployeeBundle:Company')->find($id);
        if(!$company)   {
            return new JsonResponse(['status' => 'error']);
        }
        $em->remove($company);
        $em->flush();
        return new JsonResponse(['status' => 'success']);
    }

    private function createAddForm($company) {

        $form = $this->createForm(CompanyType::class, $company, array(
            'action' => $this->generateUrl('company_save'),
        ));
        $form->add('save', SubmitType::class, array(
            'label' => 'Save Company',
            'attr'  => array('class' => 'btn btn-sm btn-success'),
        ));

        return $form;
    }

    private function createEditForm($company) {

        $form = $this->createForm(CompanyType::class, $company, array(
            'action' => $this->generateUrl('company_update', array('id' => $company->getId()))
        ));
        $form->add('save', SubmitType::class, array(
            'label' => 'Update Company',
            'attr'  => array('class' => 'btn btn-sm btn-success'),
        ));

        return $form;
    }
}