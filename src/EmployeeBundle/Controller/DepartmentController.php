<?php

namespace EmployeeBundle\Controller;

use EmployeeBundle\Entity\Department;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use EmployeeBundle\Form\DepartmentType;

/**
 * Department controller.
 *
 */
class DepartmentController extends Controller {

    /**
     * Lists all department entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $departments = $em->getRepository('EmployeeBundle:Department')->findAll();

        return $this->render('EmployeeBundle:Department:index.html.twig', array(
                    'departments' => $departments,
        ));
    }

    /**
     * Creates a new department entity.
     *
     */
    public function newAction(Request $request) {
        $department = new Department();
        $form = $this->createForm(DepartmentType::class, $department);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($department);
            $em->flush();

            return $this->redirectToRoute('department_index');
        }

        return $this->render('EmployeeBundle:Department:new.html.twig', array(
                    'department' => $department,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing department entity.
     *
     */
    public function editAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $department = $em->getRepository('EmployeeBundle:Department')->find($id);
        $editForm = $this->createForm(DepartmentType::class, $department);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            return $this->redirectToRoute('department_index');
        }

        return $this->render('EmployeeBundle:Department:edit.html.twig', array(
                    'department' => $department,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a department entity.
     *
     */
    public function deleteAction($id) {
        
        $em = $this->getDoctrine()->getManager();
        $department = $em->getRepository('EmployeeBundle:Department')->find($id);
        if ($department) {
            $em->remove($department);
            $em->flush();
        }

        return $this->redirectToRoute('department_index');
    }

}
