<?php

namespace EmployeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MiscController extends Controller {

    public function serviceAction() {

        return $this->render('EmployeeBundle:Misc:service.html.twig');
    }

    public function flashAction() {

        $this->addFlash(
                'notice', 'Your changes were saved!'
        );
        return $this->render('EmployeeBundle:Misc:flash_bag.html.twig');
    }

}
