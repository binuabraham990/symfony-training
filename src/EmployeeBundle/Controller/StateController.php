<?php

namespace EmployeeBundle\Controller;

use EmployeeBundle\Entity\State;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use EmployeeBundle\Form\StateType;
/**
 * State controller.
 *
 */
class StateController extends Controller {

    /**
     * Lists all state entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $states = $em->getRepository('EmployeeBundle:State')->findAll();

        return $this->render('EmployeeBundle:State:index.html.twig', array(
                    'states' => $states,
        ));
    }

    /**
     * Creates a new state entity.
     *
     */
    public function newAction(Request $request) {
        $state = new State();
        $form = $this->createForm(StateType::class, $state);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($state);
            $em->flush();

            return $this->redirectToRoute('state_index');
        }

        return $this->render('EmployeeBundle:State:new.html.twig', array(
                    'state' => $state,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing state entity.
     *
     */
    public function editAction(Request $request, State $state) {
        $editForm = $this->createForm(StateType::class, $state);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('state_index');
        }

        return $this->render('EmployeeBundle:State:edit.html.twig', array(
                    'state' => $state,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a state entity.
     *
     */
    public function deleteAction(Request $request, State $state) {
        if ($state) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($state);
            $em->flush();
        }

        return $this->redirectToRoute('state_index');
    }

}
