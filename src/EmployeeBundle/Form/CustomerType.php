<?php

namespace EmployeeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\{
    FormEvent,
    FormEvents,
    FormInterface
};

class CustomerType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('country', EntityType::class, array(
                    'class' => 'EmployeeBundle:Country',
                    'choice_label' => 'name',
                    'placeholder' => '-- Select --',
                ))
                ->add('state')
                ->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'))
                ->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'))
        ;
    }

    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
        $country = array_key_exists('country', $data) ? $data['country'] : '';

        $this->addState($form, $country);
    }

    function onPreSetData(FormEvent $event) {
        $data = $event->getData();
        $form = $event->getForm();
        $country = $data->getCountry() != NULL ? $data->getCountry() : '';

        $this->addState($form, $country);
    }

    protected function addState(FormInterface $form, $country) {

        $form->remove('state');
        $form->add('state', EntityType::class, array(
            'class' => 'EmployeeBundle:State',
            'choice_label' => 'name',
            'placeholder' => '- State -',
            'query_builder' => function(EntityRepository $er) use ($country) {
                return $er->createQueryBuilder('s')
                                ->where('s.country = :country')
                                ->setParameters(['country' => $country])
                ;
            }
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EmployeeBundle\Entity\Customer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'employeebundle_customer';
    }

}
